networking-fujitsu Style Commandments
=====================================

Read the OpenStack Style Commandments https://docs.openstack.org/hacking/latest/

Testing
-------

networking-fujitsu uses testtools and stestr for its unittest suite and its
test runner. If you'd like to learn more in depth:

  https://testtools.readthedocs.io/en/latest/
  https://stestr.readthedocs.io/en/latest/
